#!/bin/bash

# Add multi-arch for Wine support
sudo dpkg --add-architecture i386

# Update
sudo DEBIAN_FRONTEND=noninteractive apt-get update
sudo DEBIAN_FRONTEND=noninteractive apt-get dist-upgrade -y

# Setup firewall
sudo DEBIAN_FRONTEND=noninteractive apt-get install ufw -y
sudo ufw default deny incoming
sudo ufw default allow outgoing
sudo ufw allow in on tun0 from 10.10.10.0/24
sudo ufw allow in on tun0 from 10.129.0.0/16
sudo ufw enable

# Regen SSH host keys
sudo mkdir /etc/ssh/kali_default_keys
sudo mv /etc/ssh/ssh_host_* /etc/ssh/kali_default_keys
sudo dpkg-reconfigure openssh-server

# Install... stuff
sudo DEBIAN_FRONTEND=noninteractive apt-get install --install-suggests -y default-jdk snapd joplin joplin-cli bloodhound tmux-plugin-manager nextnet python3 python3-pip python3-venv curl enum4linux gobuster nbtscan nikto nmap onesixtyone oscanner smbclient smbmap smtp-user-enum snmpcheck sslscan sipvicious tnscmd10g whatweb wkhtmltopdf powershell-preview powershell-empire powercat webshells python-pip seclists beef-xss set veil unicornscan masscan nishang wine32

# Install OhMyZSH
sh -c "$(wget -O- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

# Set ZSH as default shell
chsh -s /bin/zsh

# Start and enable AppArmor
sudo systemctl start apparmor.service
sudo systemctl enable apparmor.service

# Start and enable snap daemon
sudo systemctl start snapd.service
sudo systemctl enable snapd.service

# Add snap/bin to PATH
echo "export PATH=$PATH:/snap/bin" | tee -a ~/.zshrc

# Install VS Code
sudo snap install --classic code-insiders

# Install python modules
python3 -m pip install --user pipx
python3 -m pipx ensurepath
python2 -m pip install termcolor
python2 -m pip install requests
python3 -m pipx install git+https://github.com/Tib3rius/AutoRecon.git
python3 -m pipx install ciphey
sudo python3 -m pip install thefuck
python3 -m pip install hexdump
python2 -m pip install enum34

# Install evil-winrm
sudo gem install evil-winrm

# Decompress rockyou
sudo gzip --decompress /usr/share/wordlists/rockyou.txt.gz

# Clone repos
mkdir ~/.repos
git clone https://github.com/gpakosz/.tmux.git ~/.repos/oh-my-tmux
git clone https://github.com/FlameOfIgnis/Pwdb-Public.git ~/.repos/Pwdb-Public
git clone https://github.com/carlospolop/privilege-escalation-awesome-scripts-suite.git ~/.repos/PEAS
git clone https://github.com/PowerShellMafia/PowerSploit.git -b dev ~/.repos/powersploit # dev branch matches commands from Bloodhound exploit info for easy copy/paste action
git clone https://github.com/BloodHoundAD/BloodHound.git ~/.repos/BloodHound

# Setup OhMyTmux
ln -s -f ~/.repos/oh-my-tmux/.tmux.conf ~/.tmux.conf
cp ~/.repos/oh-my-tmux/.tmux.conf.local ~/.tmux.conf.local

# Add symlinks for Pwdb
sudo ln -s -f ~/.repos/Pwdb-Public/wordlists/ignis-*.txt /usr/share/seclists/Passwords/

# Add symlinks for PEAS
sudo ln -s -f ~/.repos/PEAS/winPEAS/winPEASbat/winPEAS.bat /usr/share/windows-resources/binaries/
sudo ln -s -f ~/.repos/PEAS/winPEAS/winPEASexe/winPEAS/bin/Obfuscated\ Releases/*.exe /usr/share/windows-resources/binaries

# Make sure apt version of PowerSploit is removed and add symlinks for the dev branch
sudo DEBIAN_FRONTEND=noninteractive apt-get remove powersploit -y
sudo ln -s -f ~/.repos/PowerSploit /usr/share/windows-resources/PowerSploit

# Add symlink for SharpHound
sudo ln -s -f ~/.repos/BloodHound/Ingestors/SharpHound.exe /usr/share/windows-resources/binaries/

# Install discord
wget -O ~/discord.deb https://discordapp.com/api/download?platform=linux&format=deb
sudo dpkg -i ~/discord.deb
sudo DEBIAN_FRONTEND=noninteractive apt-get -f install -y
rm ~/discord.deb
rm ~/wget-log

# Add FoxyProxy to Firefox
firefox https://addons.mozilla.org/firefox/downloads/file/3616824/foxyproxy_standard-7.5.1-an+fx.xpi

# Set tmux default shell to zsh
tee -a ~/.tmux.conf.local > /dev/null <<EOT

# Set default shell to zsh
set -g default-shell /etc/zsh
EOT

# Add alias to get tun0 IP
tee -a ~/.zshrc <<EOT

# Add alias to quickly pull tun0 IP
alias gettunip="sudo ifconfig | grep 'inet 10.10.' | awk {'print \$2'}"
EOT

# Add thefuck to zshrc
tee -a ~/.zshrc <<EOT

# The fuck?
eval \$(thefuck --alias)
EOT

# Alias code to code-insiders
tee -a ~/.zshrc <<EOT

# Alias code to code-insiders
alias code='/snap/bin/code-insiders'
EOT

# Modify hosts file
sudo tee -a /etc/hosts > /dev/null <<EOT

# HackTheBox
10.10.10.3      lame.htb
10.10.10.4      legacy.htb
10.10.10.5      devel.htb
10.10.10.6      popcorn.htb
10.10.10.7      beep.htb
10.10.10.8      optimum.htb
10.10.10.9      bastard.htb
10.10.10.10     tenten.htb
10.10.10.11     arctic.htb
10.10.10.13     chronos.htb
10.10.10.14     grandpa.htb
10.10.10.15     granny.htb
10.10.10.16     october.htb
10.10.10.17     brainfuck.htb
10.10.10.18     lazy.htb
10.10.10.20     sneaky.htb
10.10.10.21     joker.htb
10.10.10.22     europa.htb
10.10.10.24     haircut.htb
10.10.10.25     holiday.htb
10.10.10.27     calamity.htb
10.10.10.29     bank.htb
10.10.10.31     charon.htb
10.10.10.34     jail.htb
10.10.10.37     blocky.htb
10.10.10.40     blue.htb
10.10.10.43     nineveh.htb
10.10.10.46     apocalyst.htb
10.10.10.47     shrek.htb
10.10.10.48     mirai.htb
10.10.10.51     solidstate.htb
10.10.10.52     mantis.htb
10.10.10.55     kotarak.htb
10.10.10.56     shocker.htb
10.10.10.57     minion.htb
10.10.10.58     node.htb
10.10.10.59     tally.htb
10.10.10.60     sense.htb
10.10.10.61     enterprise.htb
10.10.10.62     fulcrum.htb
10.10.10.63     jeeves.htb
10.10.10.64     stratosphere.htb
10.10.10.65     ariekei.htb
10.10.10.66     nightmare.htb
10.10.10.67     inception.htb
10.10.10.68     bashed.htb
10.10.10.69     fluxcapacitor.htb
10.10.10.70     canape.htb
10.10.10.71     rabbit.htb
10.10.10.72     fighter.htb
10.10.10.73     falafel.htb
10.10.10.74     chatterbox.htb
10.10.10.75     nibbles.htb
10.10.10.76     sunday.htb
10.10.10.77     reel.htb
10.10.10.78     aragog.htb
10.10.10.79     valentine.htb
10.10.10.80     crimestoppers.htb
10.10.10.81     bart.htb
10.10.10.82     silo.htb
10.10.10.83     olympus.htb
10.10.10.84     poison.htb
10.10.10.85     celestial.htb
10.10.10.86     dab.htb
10.10.10.87     waldo.htb
10.10.10.88     tartarsauce.htb
10.10.10.89     smasher.htb
10.10.10.90     dropzone.htb
10.10.10.91     devoops.htb
10.10.10.92     mischief.htb
10.10.10.93     bounty.htb
10.10.10.94     reddish.htb
10.10.10.95     jerry.htb
10.10.10.96     oz.htb
10.10.10.97     secnotes.htb
10.10.10.98     access.htb
10.10.10.100    active.htb
10.10.10.101    ghoul.htb
10.10.10.102    hawk.htb
10.10.10.103    sizzle.htb
10.10.10.104    giddy.htb
10.10.10.105    carrier.htb
10.10.10.106    ethereal.htb
10.10.10.107    ypuffy.htb
10.10.10.108    zipper.htb
10.10.10.109    vault.htb
10.10.10.110    craft.htb
10.10.10.111    frolic.htb
10.10.10.112    bighead.htb
10.10.10.113    redcross.htb
10.10.10.114    bitlab.htb
10.10.10.115    haystack.htb
10.10.10.116    conceal.htb
10.10.10.117    irked.htb
10.10.10.119    lightweight.htb
10.10.10.120    chaos.htb
10.10.10.121    help.htb
10.10.10.122    ctf.htb
10.10.10.123    friendzone.htb
10.10.10.124    flujab.htb
10.10.10.125    querier.htb
10.10.10.126    unattended.htb
10.10.10.127    fotune.htb
10.10.10.128    hackback.htb
10.10.10.129    kryptos.htb
10.10.10.130    arkham.htb
10.10.10.131    lacasadepapel.htb
10.10.10.132    helpline.htb
10.10.10.133    onetwoseven.htb
10.10.10.134    bastion.htb
10.10.10.135    smasher2.htb
10.10.10.137    luke.htb
10.10.10.138    writeup.htb
10.10.10.139    ellingson.htb
10.10.10.140    swagshop.htb
10.10.10.142    chainsaw.htb
10.10.10.143    jarvis.htb
10.10.10.144    re.htb
10.10.10.145    player.htb
10.10.10.146    networked.htb
10.10.10.147    safe.htb
10.10.10.148    rope.htb
10.10.10.149    heist.htb
10.10.10.150    curling.htb
10.10.10.151    sniper.htb
10.10.10.152    netmon.htb
10.10.10.153    teacher.htb
10.10.10.154    bankrobber.htb
10.10.10.155    scavenger.htb
10.10.10.156    zetta.htb
10.10.10.157    wall.htb
10.10.10.158    json.htb
10.10.10.159    registry.htb
10.10.10.160    postman.htb
10.10.10.161    forest.htb
10.10.10.162    mango.htb
10.10.10.163    ai.htb
10.10.10.165    traverxec.htb
10.10.10.167    control.htb
10.10.10.168    obscurity.htb
10.10.10.169    resolute.htb
10.10.10.170    playertwo.htb
10.10.10.171    openadmin.htb
10.10.10.172    monteverde.htb
10.10.10.173    patents.htb
10.10.10.174    fatty.htb
10.10.10.175    sauna.htb
10.10.10.176    book.htb
10.10.10.177    oouch.htb
10.10.10.178    nest.htb
10.10.10.179    multimaster.htb
10.10.10.180    remote.htb
10.10.10.181    traceback.htb
10.10.10.182    cascade.htb
10.10.10.183    forwardslash.htb
10.10.10.184    servmon.htb
10.10.10.185    magic.htb
10.10.10.186    quick.htb
10.10.10.187    admirer.htb
10.10.10.188    cache.htb
10.10.10.189    travel.htb
10.10.10.190    dyplesher.htb
10.10.10.191    blunder.htb
10.10.10.192    blackfield.htb
10.10.10.193    fuse.htb
10.10.10.194    tabby.htb
10.10.10.195    intense.htb
10.10.10.196    ropetwo.htb
10.10.10.197    sneakymailer.htb
10.10.10.198    buff.htb
10.10.10.199    openkeys.htb
10.10.10.200    unbalanced.htb
10.10.10.201    laser.htb
10.10.10.203    worker.htb
10.10.10.204    omni.htb
10.10.10.205    feline.htb
10.10.10.206    passage.htb
10.10.10.207    compromised.htb
10.10.10.209    doctor.htb
EOT